#!/bin/bash -xeu

aws ssm put-parameter --type SecureString --name "/${PROJECT_SHORTNAME}/dbpass" --value hasurarocks
