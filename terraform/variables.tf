variable "hasura_image" {
  description = "Docker image to run in the ECS cluster"
  default = "hasura/graphql-engine:v1.0.0-alpha31.cli-migrations:latest"
}

variable "stage" {
  description = "dev, test, staging, prod, etc."
  default = "dev"
}

variable "longname" {
  description = "long name of the app"
  default = "hasura-aws-jwt"
}

variable "shortname" {
  description = "short name of the app"
  default = "haws"
}

variable "desired_count" {
  type = "string"
  default = "1"
}

variable "domain_cert_arn" {
  type = "string"
  default = "arn:aws:acm:us-east-1:995385930844:certificate/9e25a92b-8f85-4a7e-9ac9-a8353b67708d"
}

variable "account_id" {
  type = "string"
  default = "995385930844"
}

variable "app_domain" {
  type = "string"
  default = "timeisup.net"
}

variable "hosted_zone_id" {
  type = "string"
  default = " Z3D4NUG2X20J10"
}

variable "callback_urls" {
  type = "list"
  default = [
    "https://www.timeisup.net/signedin"
  ]
}

variable "logout_urls" {
  type = "list"
  default = [
    "https://www.timeisup.net/signedout"]
}




