provider "aws" {
  region = "us-east-1"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.longname}"
  cidr = "10.0.0.0/16"

  azs = [
    "us-east-1a",
    "us-east-1b"
  ]
  private_subnets = [
    "10.0.1.0/24",
    "10.0.2.0/24"
    ]
  public_subnets = [
    "10.0.101.0/24",
    "10.0.102.0/24"
    ]

  enable_nat_gateway = true

  tags = {
    terraform = "true"
    stage = "${var.stage}"
    app = "${var.longname}"
  }
}

module "postgresql_security_group" {
  source = "terraform-aws-modules/security-group/aws//modules/postgresql"
  vpc_id = "${module.vpc.vpc_id}"
  name = "${var.shortname}-pg-sg"
  ingress_cidr_blocks = "${module.vpc.private_subnets_cidr_blocks}"
}


# use parameter -- seed ~/bin/create-secrets.sh
data "aws_ssm_parameter" "db_pass" {
  name  = "/${var.shortname}/dbpass"
  with_decryption = true
}

module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier = "${var.shortname}"

  engine = "postgres"
  engine_version = "9.6.3"
  instance_class = "db.t2.small"
  allocated_storage = 5
  storage_encrypted = false

  name = "${var.shortname}"

  username = "dbadmin"

  password = "${data.aws_ssm_parameter.db_pass.value}"
  port = "5432"

  vpc_security_group_ids = [
    "${module.postgresql_security_group.this_security_group_id}"]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window = "03:00-06:00"

  # disable backups to create DB faster
  # TODO Note the above, no backups
  backup_retention_period = 0

  tags = {
    terraform = "true"
    stage = "${var.stage}"
    app = "${var.longname}"
  }

  # DB subnet group
  subnet_ids = [
    "${module.vpc.private_subnets}"]

  # DB parameter group
  family = "postgres9.6"

  # DB option group
  major_engine_version = "9.6"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "${var.shortname}"

  # Database Deletion Protection
  deletion_protection = false
}
