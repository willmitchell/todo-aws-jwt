resource "aws_ecs_cluster" "cluster" {
  name = "c1"
}

module "global_alb" {
  source = "telia-oss/loadbalancer/aws"
  version = "0.1.0"

  name_prefix = "${var.shortname}"
  type = "application"
  internal = "false"
  vpc_id = "${module.vpc.vpc_id}"
  subnet_ids = [
    "${module.vpc.public_subnets}"]

  tags {
    stage = "${var.stage}"
    app = "${var.longname}"
    terraform = "true"
  }
}


// Use SSL always

resource "aws_lb_listener_rule" "redirect_80_apex" {
  listener_arn = "${aws_lb_listener.alb_global_listener.arn}"
  priority = 1

  action {
    type = "redirect"

    redirect {
      host = "www.${var.app_domain}"
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  condition {
    field = "host-header"
    values = [
      "${var.app_domain}"]
  }
}

resource "aws_lb_listener_rule" "redirect_80_to_443" {
  listener_arn = "${aws_lb_listener.alb_global_listener.arn}"
  priority = 2

  action {
    type = "redirect"

    redirect {
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  condition {
    field = "host-header"
    values = [
      "*.${var.app_domain}"]
  }
}


// Key inbound default forwarding rules

resource "aws_lb_listener" "alb_global_listener" {
  load_balancer_arn = "${module.global_alb.arn}"
  port = "80"
  protocol = "HTTP"

  default_action {

    type = "redirect"

    redirect {
      host = "www.${var.app_domain}"
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "alb_global_tls_listener" {
  load_balancer_arn = "${module.global_alb.arn}"
  port = "443"
  protocol = "HTTPS"
  certificate_arn = "${var.domain_cert_arn}"

  default_action {
    target_group_arn = "${module.todo.target_group_arn}"
    type = "forward"
  }
}


resource "aws_lb_listener_rule" "redirect_443_apex" {
  listener_arn = "${aws_lb_listener.alb_global_tls_listener.arn}"
  priority = 1

  action {
    type = "redirect"

    redirect {
      host = "www.${var.app_domain}"
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  condition {
    field = "host-header"
    values = [
      "${var.app_domain}"]
  }
}


resource "aws_security_group_rule" "alb_ingress_80" {
  security_group_id = "${module.global_alb.security_group_id}"
  type = "ingress"
  protocol = "tcp"
  from_port = "80"
  to_port = "80"
  cidr_blocks = [
    "0.0.0.0/0"]
  ipv6_cidr_blocks = [
    "::/0"]
}
resource "aws_security_group_rule" "alb_ingress_443" {
  security_group_id = "${module.global_alb.security_group_id}"
  type = "ingress"
  protocol = "tcp"
  from_port = "443"
  to_port = "443"
  cidr_blocks = [
    "0.0.0.0/0"]
  ipv6_cidr_blocks = [
    "::/0"]
}
