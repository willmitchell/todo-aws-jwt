resource "aws_lb_listener_rule" "todo_host_rule1" {
  listener_arn = "${aws_lb_listener.alb_global_tls_listener.arn}"
  priority = 50

  action {
    type = "forward"
    target_group_arn = "${module.todo.target_group_arn}"
  }

  condition {
    field = "host-header"
    values = [
      "www.${var.app_domain}"
    ]
  }
}


resource "aws_security_group_rule" "task_ingress_3000" {
  security_group_id = "${module.todo.service_sg_id}"
  type = "ingress"
  protocol = "tcp"
  from_port = "3000"
  to_port = "3000"
  source_security_group_id = "${module.global_alb.security_group_id}"
}

module "todo" {
  source = "app-fargate-svc"

  name_prefix = "todo"
  vpc_id = "${module.vpc.vpc_id}"
  private_subnet_ids = "${module.vpc.private_subnets}"
  cluster_id = "${aws_ecs_cluster.cluster.id}"
  task_container_image = "registry.gitlab.com/willmitchell/hasura-aws-jwt/todo:latest"
  desired_count = "${var.desired_count}"

  task_container_environment_count = "3" // TODO UPDATE  HEY YOU
  task_container_environment = {
    NONCE=8
    PORT= "3000"
    ROOT_API = "https://app.${var.app_domain}"
  }

  // public ip is needed for default vpc, default is false
  task_container_assign_public_ip = "true"

  // port, default protocol is HTTP
  task_container_port = "3000"

  service_discovery_service_arn = "${aws_service_discovery_service.todo_sds.arn}"

  health_check {
    port = "traffic-port"
    path = "/"
  }

  tags {
    environment = "test"
    terraform = "true"
  }

  lb_arn = "${module.global_alb.arn}"
}

resource "aws_service_discovery_service" "todo_sds" {
  "dns_config" {
    "dns_records" {
      ttl = 10
      type = "A"
    }
    namespace_id = "${aws_service_discovery_private_dns_namespace.private_dns_ns.id}"
  }
  health_check_custom_config {
    failure_threshold = 1
  }
  name = "todo"
}
