resource "aws_lb_listener_rule" "hasura_host_rule" {
  listener_arn = "${aws_lb_listener.alb_global_tls_listener.arn}"
  priority = 20

  action {
    type = "forward"
    target_group_arn = "${module.hasura.target_group_arn}"
  }

  condition {
    field = "host-header"
    values = [
      "graphql.${var.app_domain}"]
  }
}

resource "aws_lb_listener_rule" "hasura_host_rule2" {
  listener_arn = "${aws_lb_listener.alb_global_tls_listener.arn}"
  priority = 22

  action {
    type = "forward"
    target_group_arn = "${module.hasura.target_group_arn}"
  }

  condition {
    field = "host-header"
    values = [
      "www.${var.app_domain}"
    ]
  }

  condition {
    field = "path-pattern"
    values = [
      "/v1/*",
    ]
  }
}
resource "aws_lb_listener_rule" "hasura_host_rule3" {
  listener_arn = "${aws_lb_listener.alb_global_tls_listener.arn}"
  priority = 23

  action {
    type = "forward"
    target_group_arn = "${module.hasura.target_group_arn}"
  }

  condition {
    field = "host-header"
    values = [
      "www.${var.app_domain}"
    ]
  }

  condition {
    field = "path-pattern"
    values = [
      "/console/*",
    ]
  }
}

resource "aws_lb_listener_rule" "hasura_host_rule4" {
  listener_arn = "${aws_lb_listener.alb_global_tls_listener.arn}"
  priority = 24

  action {
    type = "forward"
    target_group_arn = "${module.hasura.target_group_arn}"
  }

  condition {
    field = "host-header"
    values = [
      "www.${var.app_domain}"
    ]
  }

  condition {
    field = "path-pattern"
    values = [
      "/v1alpha1/graphql/*",
    ]
  }
}

resource "aws_security_group_rule" "task_ingress_hasura_8080" {
  security_group_id = "${module.hasura.service_sg_id}"
  type = "ingress"
  protocol = "tcp"
  from_port = "8080"
  to_port = "8080"
  source_security_group_id = "${module.global_alb.security_group_id}"
}

data "template_file" "jwt_descriptor" {
  template = <<JSON
{
  "type":             "RS256",
  "jwk_url":          "https://cognito-idp.us-east-1.amazonaws.com/${aws_cognito_user_pool.pool.id}/.well-known/jwks.json",
  "claims_format":    "stringified_json"
}
JSON
}

module "hasura" {
  source = "app-fargate-svc"

  name_prefix = "hasura"
  vpc_id = "${module.vpc.vpc_id}"
  private_subnet_ids = "${module.vpc.private_subnets}"
  cluster_id = "${aws_ecs_cluster.cluster.id}"
  task_container_image = "registry.gitlab.com/willmitchell/hasura-aws-jwt/hasura:latest"
  desired_count = "${var.desired_count}"

  task_container_environment_count = "5"
  task_container_environment = {
    HASURA_GRAPHQL_JWT_SECRET = "${data.template_file.jwt_descriptor.rendered}"
    HASURA_GRAPHQL_ACCESS_KEY = "hello"
    HASURA_GRAPHQL_ENABLE_CONSOLE = "yes"
    HASURA_GRAPHQL_MIGRATIONS_DIR = "/migrations"
    HASURA_GRAPHQL_DATABASE_URL = "postgres://${module.db.this_db_instance_username}:${data.aws_ssm_parameter.db_pass.value}@${module.db.this_db_instance_address}:${module.db.this_db_instance_port}/${module.db.this_db_instance_name}"
  }

  // public ip is needed for default vpc, default is false
  task_container_assign_public_ip = "true"

  // port, default protocol is HTTP
  task_container_port = "8080"

  service_discovery_service_arn = "${aws_service_discovery_service.hasura_sds.arn}"

  health_check {
    port = "traffic-port"
    path = "/console"
  }

  tags {
    environment = "test"
    terraform = "true"
  }

  lb_arn = "${module.global_alb.arn}"
}

resource "aws_service_discovery_service" "hasura_sds" {
  "dns_config" {
    "dns_records" {
      ttl = 10
      type = "A"
    }
    namespace_id = "${aws_service_discovery_private_dns_namespace.private_dns_ns.id}"
  }
  health_check_custom_config {
    failure_threshold = 1
  }
  name = "hasura"
}
