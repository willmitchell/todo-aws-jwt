//
// Route53 Service Discovery
//
resource "aws_service_discovery_private_dns_namespace" "private_dns_ns" {
//  count       = "${module.enabled.value && ! module.enable_public_namespace.value ? 1 : 0}"
  name        = "${var.shortname}.local"
  description = "Service Discovery"
  vpc         = "${module.vpc.vpc_id}"
}



// Public DNS


data "aws_route53_zone" "app_zone" {
  name = "${var.app_domain}."
  private_zone = false
}

// Create a bunch of A Alias Records and point them ALL to the global ALB.

resource "aws_route53_record" "www" {
  zone_id = "${data.aws_route53_zone.app_zone.zone_id}"
  name = "www.${data.aws_route53_zone.app_zone.name}"
  type = "A"

  alias {
    name = "${module.global_alb.dns_name}"
    zone_id = "${module.global_alb.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "apex" {
  zone_id = "${data.aws_route53_zone.app_zone.zone_id}"
  name = "${data.aws_route53_zone.app_zone.name}"
  type = "A"

  alias {
    name = "${module.global_alb.dns_name}"
    zone_id = "${module.global_alb.zone_id}"
    evaluate_target_health = true
  }
}

// Service specific

resource "aws_route53_record" "graphql" {
  zone_id = "${data.aws_route53_zone.app_zone.zone_id}"
  name = "graphql.${data.aws_route53_zone.app_zone.name}"
  type = "A"

  alias {
    name = "${module.global_alb.dns_name}"
    zone_id = "${module.global_alb.zone_id}"
    evaluate_target_health = true
  }
}
resource "aws_route53_record" "app" {
  zone_id = "${data.aws_route53_zone.app_zone.zone_id}"
  name = "app.${data.aws_route53_zone.app_zone.name}"
  type = "A"

  alias {
    name = "${module.global_alb.dns_name}"
    zone_id = "${module.global_alb.zone_id}"
    evaluate_target_health = true
  }
}
resource "aws_route53_record" "admin" {
  zone_id = "${data.aws_route53_zone.app_zone.zone_id}"
  name = "admin.${data.aws_route53_zone.app_zone.name}"
  type = "A"

  alias {
    name = "${module.global_alb.dns_name}"
    zone_id = "${module.global_alb.zone_id}"
    evaluate_target_health = true
  }
}
