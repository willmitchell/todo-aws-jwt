# Integrating Todo app with AWS Cognito User Pools and JWT authorization with Hasura GraphQL Engine

In this example, we use Hasura GraphQL engine's JWT authorization mode. We use
AWS Cognito as our authentication and JWT token provider.

## Prerequisites

- [Terraform](https://terraform.io) CLI installed
- AWS account
- [Direnv recommended](https://direnv.net)

## Variables

- cp .envrc.sample .envrc && vi .envrc ## modify to taste
- cd terraform && cp variables.sample variables.tf

## Create AWS infrastructure

- cd $PROJECT_ROOT/terraform
- terraform init
- terraform apply

TODO the rest, work in progress
